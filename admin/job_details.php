<?php
session_start();
require_once("../vendor/autoload.php");
include("../templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\Job_circular;
$object=new Job_circular();
$object->prepareData($_GET);
$data=$object->showOne();
$post_date=date("d-M-Y", strtotime("$data->post_date"));
$deadline=date("d-M-Y", strtotime("$data->deadline"));
$allData=$object->show();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include("../templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("../templateLayout/adminNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Job</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Job</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row page-row">
                    <div class="news-wrapper col-md-8">
                        <article class="news-item">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Edit</button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal2" style="background-color: #ef110a;border-color: #ef110a;">Delete</button>
                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit Job Details</h4>
                                        </div>
                                        <div class="modal-body">
                                            <article class="contact-form col-md-12 col-sm-12 col-xs-12  page-row">
                                                <form action="../controller/update_job.php" method="post">
                                                    <div class="form-group name">
                                                        <label>Title </label>
                                                        <input type="text" class="form-control" placeholder="Job Title" name="title" value="<?php echo $data->title?>"  required>
                                                    </div><!--//form-group-->
                                                    <div class="form-group name">
                                                        <label>Company Name </label>
                                                        <input type="text" class="form-control" placeholder="Company Name" name="company_name" value="<?php echo $data->company_name?>" required>
                                                    </div><!--//form-group-->
                                                    <div class="form-group name">
                                                        <label>Company Contact </label>
                                                        <input type="number" class="form-control" placeholder="Company Contact " name="company_contact" value="<?php echo $data->company_contact?>" required>
                                                    </div><!--//form-group-->
                                                    <div class="form-group name">
                                                        <label>Company Address </label>
                                                        <input type="text" class="form-control" placeholder="Company Address " name="company_address" value="<?php echo $data->company_address?>" required>
                                                    </div><!--//form-group-->
                                                    <div class="form-group name">
                                                        <label>Position</label>
                                                        <input type="text" class="form-control" placeholder="Position" name="post" value="<?php echo $data->post?>"  required>
                                                    </div><!--//form-group-->
                                                    <div class="form-group name">
                                                        <label>vacancy</label>
                                                        <input type="text" class="form-control" placeholder="Number of vacancy" name="vacancy" value="<?php echo $data->vacancy?>"  required>
                                                    </div><!--//form-group-->
                                                    <div class="form-group name">
                                                        <label>Link</label>
                                                        <input type="text" class="form-control" placeholder="Job web link" name="link" value="<?php echo $data->link?>"  required>
                                                    </div><!--//form-group-->
                                                    <div class="form-group name">
                                                        <label>Deadline</label>
                                                        <input type="text" class="form-control date_field" placeholder="Deadline" name="deadline" value="<?php echo $data->deadline?>"  required>
                                                    </div><!--//form-group-->
                                                    <div class="form-group name">
                                                        <label>Requirement</label>
                                                        <textarea id="text" rows="5" name="requirement" class="form-control" placeholder="Job Requirement" required><?php echo $data->requirement?></textarea>
                                                    </div><!--//form-group-->
                                                    <input type="hidden" value="<?php echo $data->id?>" name="id">
                                                    <button type="submit" class="btn btn-theme" >Update Job</button>
                                                </form>
                                            </article>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="myModal2" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Delete This Job</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-md-12">
                                                <form action="../controller/deleteJob.php" method="post">
                                                    <p>Are you sure to delete this job?</p>
                                                    <input type="hidden" value="<?php echo $data->id?>" name="id">
                                                    <input type="submit" class="btn btn-primary" value="Delete">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>




                            <div>
                                <h4><strong><a href="job_details.php?id=<?php echo $data->id?>"><br><?php echo $data->title?></a></strong></h4>
                                <p>Posted: <?php echo $post_date;?></p>
                                <h5><strong>Company: <a><?php echo $data->company_name?></a></strong></h5>
                                <h6>Post: <a><?php echo $data->post?></a></h6>
                                <h6>Deadline: <a><?php echo $deadline?></a></h6>
                                <h6>Contact: <a><?php echo $data->company_contact?></a></h6>

                                <br>
                            </div>
                            <h5><strong>Job vacancy:</strong> <?php echo $data->vacancy?></h5><br>
                            <h5><strong>Job Requirements</a></strong></h5>
                            <h6><a><?php echo $data->requirement?></a></h6>
                            <a href="<?php echo $data->link?>" target="_blank" class="btn btn-primary">Job Link</a>
                        </article><!--//news-item-->
                    </div><!--//news-wrapper-->
                    <aside class="page-sidebar  col-md-4">
                        <section class="widget has-divider">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <br> <h3 class="title">Other Jobs</h3>
                                <?php
                                foreach ($allData as $oneData){
                                    ?>
                                    <hr>
                                    <article class="news-item">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class=" col-md-12">
                                                    <div class="details">
                                                        <h5><a href="job_details.php?id=<?php echo $oneData->id?>"><?php echo $oneData->title?></a></h5>
                                                        <h6>Company: <?php echo $oneData->company_name?></h6>
                                                        <h6>Vacancy: <?php echo $oneData->vacancy?></h6>
                                                        <h6>Deadline: <?php echo $oneData->deadline?></h6>
                                                        <a href="job_details.php?id=<?php echo $oneData->id?>" class="btn btn-primary">See job details</a>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>


                                    </article><!--//news-item-->

                                    <?php
                                }
                                ?>
                            </div>

                        </section><!--//widget-->
                    </aside>
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("../templateLayout/footer.php");?>

<?php include("../templateLayout/script/templateScript.php");?>
<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
<script> CKEDITOR.replace( 'text' );</script>

</body>
</html>

