<?php
session_start();
require_once("../vendor/autoload.php");
include("../templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==1){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include("../templateLayout/css/templateCss.php");?>
    <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/croppie.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="resources/assets/css/thumbnail-gallery.css">
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("../templateLayout/adminNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Add Category</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Add Category</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                    }

                    ?>
                    <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2 col-xs-12 col-xs-offset-0">
                        <article class="contact-form col-md-12 col-sm-12 col-xs-12  page-row">
                            <div class="panel panel-default">
                                <div class="panel-heading">Image Upload</div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <strong>Select Image:</strong>
                                            <br/>
                                            <div class="form-group">
                                                <input type="file" class="form-control" id="upload">
                                            </div>

                                            <br/>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                                            <div id="upload-demo" style="width:100%;margin: 0 auto"></div>
                                            <button class="btn btn-success upload-result">Crop Image</button>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 35px">
                                                <div id="upload-demo-i" style="background:#e1e1e1;width:66%;padding:0px;min-height:190px;margin: 0 auto;"></div>
                                            </div>
                                        </div>


                                    </div>

                                </div>



                            </div>
                            <form action="../controller/add_category.php" method="post">
                                <div id="upload-demo-i2"></div>
                                <div class="form-group name">
                                    <label for="name">Category Name </label>
                                    <input id="name" type="text" class="form-control" placeholder="Category Name" name="category_name"  required>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label for="name">Objective </label>
                                    <textarea name="objective" rows="5" class="form-control" placeholder="Add Objective" required></textarea>
                                </div><!--//form-group-->
                                <div class="form-group name">
                                    <label for="name">Proclamation</label>
                                    <textarea name="proclamation" rows="5" class="form-control" placeholder="Add Proclamation" required></textarea>
                                </div><!--//form-group-->
                                <button type="submit" class="btn btn-theme" >Add Category</button>
                            </form>
                        </article><!--//contact-form-->
                    </div>

                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("../templateLayout/footer.php");?>

<script type="text/javascript">
    $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {
            width: 300,
            height: 300,
            type: 'rectangle'
        },
        boundary: {
            width: 320,
            height: 320
        }
    });

    $('#upload').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });

        }
        reader.readAsDataURL(this.files[0]);
    });

    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {

            $.ajax({
                url: "ajaxpro.php",
                type: "POST",
                data: {"image":resp},
                dataType: 'json',
                success: function (data) {
                    html = '<img src="' + resp + '" class="img-responsive" width="100%"/>';
                    html2 = '<input type="hidden" name="pic" value="' + data + '" required/>';
                    $("#upload-demo-i").html(html);
                    $("#upload-demo-i2").html(html2);

                }
            });
        });
    });

</script>
<?php include("../templateLayout/script/templateScript.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
</body>
</html>

