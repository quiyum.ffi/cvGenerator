<script>
    $(document).ready(function() {
        var max_fields      = 1000; //maximum input boxes allowed
        var wrapper         = $("#input_fields_wrap"); //Fields wrapper
        var add_button      = $("#add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><div class="col-md-2"><div class="form-group"><input type="text" name="degree[]" class="form-control" placeholder="Degree" required /></div></div><div class="col-md-2"><div class="form-group"><input type="text" name="institute[]" class="form-control" placeholder="Institute" required /></div></div><div class="col-md-2"><div class="form-group"><input type="text" class="form-control" name="department[]" placeholder="Department/Group" required /></div></div><div class="col-md-1"><div class="form-group"><input type="text" name="board[]" class="form-control" placeholder="Board" required /></div></div><div class="col-md-1"><div class="form-group"><input type="text" name="cgpa[]" class="form-control" placeholder="CGPA" required /></div></div><div class="col-md-1"><div class="form-group"><input type="text" name="out_of[]" class="form-control" placeholder="5 or 4" required /></div></div><div class="col-md-2"><div class="form-group"><input type="number" class="form-control" name="passing_year[]" placeholder="Passign Year" required /></div></div><a href="#" class="remove_field btn btn-danger col-md-1 col-lg-1"><i class="fa fa-times" aria-hidden="true"></i></a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>

<script type="text/javascript">
    function valid() {
        var city=document.getElementById("city").value;
        if(city=="selectProduct")
        {
            alert("All fields need to fill up!");
            return false;
        }
    }
</script>
