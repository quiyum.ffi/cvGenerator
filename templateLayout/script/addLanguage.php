<script>
    $(document).ready(function() {
        var max_fields      = 1000; //maximum input boxes allowed
        var wrapper         = $("#input_fields_wrap4"); //Fields wrapper
        var add_button      = $("#add_field_button4"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><div class="col-md-5"><div class="form-group"><input type="text" class="form-control" name="language[]" placeholder="Language" required /></div></div><div class="col-md-3"><div class="form-group"><select class="form-control" name="performance[]"><option value="Good">Good</option><option value="Medium">Medium</option><option value="Weak">Weak</option></select></div></div><div class="col-md-3"><div class="form-group"><select class="form-control" name="criteria[]"><option value="Speak">Speak</option><option value="Writing">Writing</option><option value="Speak & writing both">Speak & writing both</option></select></div></div><a href="#" class="remove_field4 btn btn-danger col-md-1 col-lg-1"><i class="fa fa-times" aria-hidden="true"></i></a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field4", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>

