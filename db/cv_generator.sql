-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 04, 2017 at 09:30 AM
-- Server version: 10.2.3-MariaDB-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cv_generator`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `objective` text NOT NULL,
  `proclamation` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `pic` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `objective`, `proclamation`, `status`, `pic`) VALUES
(1, 'Medical', 'Obtaining for the challenging and responsible post of the medical officer to perform multiple responsibilities and solve patient\'s problems.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512378062.png'),
(2, 'Engineering', 'I am looking for an opportunity in a reputed organization which will help me deliver my best and upgrade my skills in engineering and meet the demands of the organization.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512378006.png'),
(3, 'Teaching', 'zxczxczxcz  xccvsxzcsxcddxcsdc  fdgdfdfsd dvdgdgdfdfd zxczxczxcz  xccvsxzcsxcddxcsdc  fdgdfdfsd dvdgdgdfdfdzxczxczxcz  xccvsxzcsxcddxcsdc  fdgdfdfsd dvdgdgdfdfdzxczxczxcz  xccvsxzcsxcddxcsdc  fdgdfdfsd dvdgdgdfdfd', 'sdsdsdws fgrfgrgr gjyjhyjukilkl;,mgv  fgrgfvgffdvfcv cv sdsdsdws fgrfgrgr gjyjhyjukilkl;,mgv  fgrgfvgffdvfcv cvsdsdsdws fgrfgrgr gjyjhyjukilkl;,mgv  fgrgfvgffdvfcv cvsdsdsdws fgrfgrgr gjyjhyjukilkl;,mgv  fgrgfvgffdvfcv cv', 1, '1512155063.png'),
(4, 'dfdf', 'dfdfdf hysgdygs ytvsdtvs chsvdtfstdbvb sydgvy svdtyfstydv hvbdygvsdvb', 'dfdfdf hysgdygs ytvsdtvs chsvdtfstdbvb sydgvy svdtyfstydv hvbdygvsdvbdfdfdf hysgdygs ytvsdtvs chsvdtfstdbvb sydgvy svdtyfstydv hvbdygvsdvbdfdfdf hysgdygs ytvsdtvs chsvdtfstdbvb sydgvy svdtyfstydv hvbdygvsdvbhysgdygs ytvsdtvs chsvdtfstdbvb sydgvy svdtyfstydv hvbdygvsdvb', 1, '1512374631.png'),
(5, 'NGO', 'A Public relations degree and internship experience in advertising provide a solid basis on which to build my communication experience and contribute to your organization.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377112.png'),
(6, 'ACCOUNTING', 'To engage in a career that will allow for progress in terms of expertise, socio-economic development, and innovation through exposure to new ideas for professional growth, as well as growth of the company.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377199.png'),
(7, 'GARMENTS', 'Obtain garment merchandiser position with ABC Store that will benefit from good communication skills, knowledge of juniorsâ€™ styles and sizes, and helpful attitude.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377268.png'),
(8, 'CUSTOMER CARE', 'Obtain a customer service position at ABC institute where I can maximize my people oriented experience, communication skills and my problem analysis and problem solving abilities.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377304.png'),
(9, 'SALESMAN', 'Obtain a position that will enable me to use my strong sales skills, marketing background and abilities to work well with people.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377350.png'),
(10, 'IT', 'Seeking a position in life to utilize my skills and abilities and achieve professional growth while being resourceful, innovative and flexible. To add valuable assets to your esteemed organization as an active member.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377386.png'),
(11, 'TELECOMMUNICATION', ' Seeking telecommunications technician role at ABC Company to apply certification in data/telecom, sharp attention to detail, and good communication skills.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377414.png'),
(12, 'Teaching', 'To obtain a position in special education teaching that will allow me to use my strong passion for student development coupled with skills and experience that will enable me to make a difference at ABC School.\r\n', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377619.png'),
(13, 'BANKING', 'Enthusiastic individual looking forward to serving as a Finance intern in XYZ Bank where I can learn and employ practical application of finance, investment, customer dealing, and business for the benefit of the organization.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377676.png'),
(14, 'RECEPTIONIST', 'An enthusiastic and committed receptionist seeking a position with XYZ company to bring professionalism and poise to their front line. Proven efficiency in operating a multi-line telephone system, providing clerical support and dealing capably with customers and queries. Detail-orientated and highly organized with a desire to meet and exceed visitor expectations', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377731.png'),
(15, 'MEDIA', 'To gain entry-level media experience where creative initiative, ideas and a genuine enthusiasm would allow me to progress.', 'I do hereby declare that the particulars provide here are true and no misinformation is given.', 0, '1512377826.png');

-- --------------------------------------------------------

--
-- Table structure for table `computer_literacy`
--

CREATE TABLE `computer_literacy` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `literacy` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `computer_literacy`
--

INSERT INTO `computer_literacy` (`id`, `user_id`, `literacy`) VALUES
(7, 76, 'hkjhkh'),
(98, 2445, 'sfasfsgg'),
(99, 2, 'safsfsdfggggf'),
(100, 2, 'babul'),
(101, 2, 'ffdfsa'),
(103, 1, 'dfgfdgfgfg');

-- --------------------------------------------------------

--
-- Table structure for table `education_qualification`
--

CREATE TABLE `education_qualification` (
  `id` int(11) NOT NULL,
  `degree` varchar(255) NOT NULL,
  `institute` varchar(255) NOT NULL,
  `board` varchar(255) DEFAULT NULL,
  `cgpa` varchar(255) NOT NULL,
  `out_of` varchar(255) NOT NULL,
  `passing_year` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `education_qualification`
--

INSERT INTO `education_qualification` (`id`, `degree`, `institute`, `board`, `cgpa`, `out_of`, `passing_year`, `department`, `user_id`) VALUES
(1, 'S.S.C.', 'Chittagong Ideal High School', 'Chittagong', '4.00', '5', '2008', 'Science', 1),
(2, 'Diploma in Engineering', 'Bangladesh Sweden Polytechnic Institute', 'Technical', '3.22', '4', '2013', 'Computer Technology', 1),
(3, 'B.Sc. Engineering', 'Port City International University', '-', '3.44', '4', '2017', 'Computer Science & Engineering', 1),
(4, 'SSC', 'Shitakondo High School', 'Chittagong', '5.00', '5', '2000', 'Science', 2),
(5, 'HSC', 'Mirssoroy Digree Collage', 'Chittagong', '4.00', '4', '2008', 'Polytical Science', 2);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hobbies` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `user_id`, `hobbies`) VALUES
(13, 2, 'misho'),
(18, 1, 'scvxx, dhbnvud, hdbnuvchduc');

-- --------------------------------------------------------

--
-- Table structure for table `job_circular`
--

CREATE TABLE `job_circular` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `company_name` text NOT NULL,
  `company_contact` text NOT NULL,
  `company_address` text NOT NULL,
  `post` text NOT NULL,
  `requirement` longtext NOT NULL,
  `vacancy` int(11) NOT NULL,
  `link` text NOT NULL,
  `deadline` date NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_circular`
--

INSERT INTO `job_circular` (`id`, `title`, `company_name`, `company_contact`, `company_address`, `post`, `requirement`, `vacancy`, `link`, `deadline`, `post_date`, `status`) VALUES
(2, 'Senior Software Architect Job ', 'IMpulse (BD) Ltd', '01826132308', 'Dhaka', 'Senior Software Architect', '<ul>\r\n	<li>Minimum B.Sc. in Computer Science from reputed local or international university</li>\r\n	<li>Full capacity to visualize, analyse and solve complex problems</li>\r\n	<li>Excellent skills to complete individual tasks with a thorough, detailed orientation</li>\r\n	<li>Fully competent to manage multiple projects or tasks at the same time</li>\r\n	<li>Strong critical thinking and problem solving skills</li>\r\n	<li>In-depth knowledge of and experience with debugging skills</li>\r\n	<li>Expert experience with maintaining proper development hygiene</li>\r\n	<li>Experienced in leveraging existing code</li>\r\n	<li>Hands-on solid experience in ASP.Net MVC or ASP.Net MVC Core with Web API</li>\r\n	<li>Broad knowledge and experience with authentication methods (OAuth, SAML etc.)</li>\r\n	<li>Solid knowledge and experience in JavaScript &amp; Angular JS</li>\r\n	<li>Familiar with CSS &amp; Bootstrap</li>\r\n	<li>Good knowledge in Oracle</li>\r\n	<li>Knowledge on PostgreSQL Database will be a Plus</li>\r\n	<li>Awareness and experience in managing application security</li>\r\n	<li>Familiar with agile processes &amp; version control system, svn is preferred</li>\r\n	<li>Experience in building and deploying scalable cloud solutions</li>\r\n	<li>Strong knowledge of software architecture and design patterns</li>\r\n	<li>Experience in designing and managing enterprise applications</li>\r\n	<li>Excellent command of written and spoken English</li>\r\n	<li>Experience of working on international projects</li>\r\n	<li>Strong leadership skills</li>\r\n	<li>Excellent client facing and internal communication skills</li>\r\n</ul>\r\n', 5, 'http://jobs.bdjobs.com/jobdetails.asp?id=738394&fcatId=8&ln=1', '2017-12-15', '2017-12-01 12:22:06', 0),
(3, 'Senior Software Engineer (.NET)', 'Datapath', '45454545', 'Dhaka', 'Senior Software Engineer (.NET)', '<ul>\r\n	<li>Full capacity to visualize, analyse and solve complex problems</li>\r\n	<li>Excellent skills to complete individual tasks with a thorough, detailed orientation</li>\r\n	<li>Fully competent to manage multiple projects or tasks at the same time</li>\r\n	<li>Strong critical thinking and problem solving skills</li>\r\n	<li>In-depth knowledge of and experience with debugging skills</li>\r\n	<li>Expert experience with maintaining proper development hygiene</li>\r\n	<li>Experienced in leveraging existing code</li>\r\n	<li>Hands-on solid experience in ASP.Net MVC or ASP.Net MVC Core with Web API</li>\r\n	<li>Broad knowledge and experience with authentication methods (OAuth, SAML etc.)</li>\r\n	<li>Solid knowledge and experience in JavaScript &amp; Angular JS</li>\r\n	<li>Familiar with CSS &amp; Bootstrap</li>\r\n	<li>Good knowledge in Oracle</li>\r\n	<li>Knowledge on PostgreSQL Database will be a Plus</li>\r\n	<li>Awareness and experience in managing application security</li>\r\n	<li>Familiar with agile processes &amp; version control system, svn is preferred</li>\r\n	<li>Experience in building and deploying scalable cloud solutions</li>\r\n	<li>Strong knowledge of software architecture and design patterns</li>\r\n	<li>Experience in designing and managing enterprise applications</li>\r\n	<li>Excellent command of written and spoken English</li>\r\n	<li>Experience of working on international projects</li>\r\n	<li>Strong leadership skills</li>\r\n	<li>Excellent client facing and internal communication skills</li>\r\n</ul>\r\n', 6, 'http://jobs.bdjobs.com/jobdetails.asp?id=738470&fcatId=8&ln=1', '2017-12-29', '2017-12-01 13:26:18', 0),
(4, 'Senior Trainer', 'Centre for Mass Education in Science (CMES)', '01812222222', 'Chittagong', 'Senior Trainer', '<ul>\r\n	<li>Masters degree in any subject but preferably candidates with degree in education.</li>\r\n	<li>Experienced to design and implement innovative learning intervention, including teachers training, technical support to the teachers</li>\r\n	<li>Good knowledge in material development/review and monitoring &amp; evaluation and report writing</li>\r\n	<li>Sound competence in English</li>\r\n	<li>Familiarity with education programme in Bangladesh</li>\r\n	<li>More for exceptional candidates, and opportunity for rapid enhancement for them.</li>\r\n</ul>\r\n', 23, 'http://jobs.bdjobs.com/jobdetails.asp?id=738651&fcatId=4&ln=1', '2017-12-30', '2017-12-04 08:12:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language_communication`
--

CREATE TABLE `language_communication` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `performance` varchar(255) NOT NULL,
  `critera` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language_communication`
--

INSERT INTO `language_communication` (`id`, `user_id`, `language`, `performance`, `critera`) VALUES
(2335, 2, 'english', 'Good', 'Speak'),
(2336, 2, 'bangla', 'Good', 'Speak'),
(2340, 1, 'Bangla', 'Medium', 'Speak'),
(2341, 1, 'English', 'Good', 'Speak');

-- --------------------------------------------------------

--
-- Table structure for table `new_field`
--

CREATE TABLE `new_field` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `new_field`
--

INSERT INTO `new_field` (`id`, `user_id`, `title`, `content`) VALUES
(1, 1, 'Reference', '<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>Sawmitra Das</p>\r\n\r\n			<p>Chairman, CSE</p>\r\n\r\n			<p>PCIU</p>\r\n			</td>\r\n			<td>\r\n			<p>Anupriya Barua</p>\r\n\r\n			<p>Lecturer, CSE</p>\r\n\r\n			<p>PCIU</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `name`, `email`, `password`, `reg_date`, `status`) VALUES
(1, 'Emonz', 'a@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-10-14 21:09:19', 0),
(2, 'Abul Hossen', 'ahossain@gmail.com', 'c73d78a220dbccdf2a26643e9812cac4', '2017-10-19 10:01:16', 0),
(3, 'admin', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-11-12 05:49:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `present_address` varchar(255) NOT NULL,
  `permanent_address` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `n_id` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `religion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `full_name`, `user_id`, `father_name`, `mother_name`, `present_address`, `permanent_address`, `contact`, `n_id`, `picture`, `gender`, `religion`) VALUES
(2, 'Mohammed Imran Hossain', 1, 'cvcvcvc', 'cvcvcv', 'cvcvcvcv', 'vcvcvcv', '343434343434', '3434343434', '1512368131.png', 'Male', 'cvcvcvcvc'),
(3, 'Abul Hossen ali', 2, 'Abul Mal', 'Hasina Begum', 'kaptai,rangamati', 'Potia ', '018893829333', '193893333919842987', '1508439148.png', 'Male', 'Islam');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `computer_literacy`
--
ALTER TABLE `computer_literacy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education_qualification`
--
ALTER TABLE `education_qualification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_circular`
--
ALTER TABLE `job_circular`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language_communication`
--
ALTER TABLE `language_communication`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_field`
--
ALTER TABLE `new_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `computer_literacy`
--
ALTER TABLE `computer_literacy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `education_qualification`
--
ALTER TABLE `education_qualification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `job_circular`
--
ALTER TABLE `job_circular`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `language_communication`
--
ALTER TABLE `language_communication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2342;
--
-- AUTO_INCREMENT for table `new_field`
--
ALTER TABLE `new_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
