<?php
session_start();
require_once("vendor/autoload.php");
include("templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\Job_circular;
$object=new Job_circular();
$object->prepareData($_GET);
$data=$object->showOne();
$post_date=date("d-M-Y", strtotime("$data->post_date"));
$deadline=date("d-M-Y", strtotime("$data->deadline"));
$allData=$object->show();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Job</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Job</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row page-row">
                    <div class="news-wrapper col-md-8">
                        <article class="news-item">
                            <div>
                                <h4><strong><a href="job_details.php?id=<?php echo $data->id?>"><br><?php echo $data->title?></a></strong></h4>
                                <p>Posted: <?php echo $post_date;?></p>
                                <h5><strong>Company: <a><?php echo $data->company_name?></a></strong></h5>
                                <h6>Post: <a><?php echo $data->post?></a></h6>
                                <h6>Deadline: <a><?php echo $deadline?></a></h6>
                                <h6>Contact: <a><?php echo $data->company_contact?></a></h6>

                                <br>
                            </div>
                            <h5><strong>Job vacancy:</strong> <?php echo $data->vacancy?></h5><br>
                            <h5><strong>Job Requirements</a></strong></h5>
                            <h6><a><?php echo $data->requirement?></a></h6>
                            <a href="<?php echo $data->link?>" target="_blank" class="btn btn-primary">Job Link</a>
                        </article><!--//news-item-->
                    </div><!--//news-wrapper-->
                    <aside class="page-sidebar  col-md-4">
                        <section class="widget has-divider">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <br> <h3 class="title">Other Jobs</h3>
                                <?php
                                foreach ($allData as $oneData){
                                    ?>
                                    <hr>
                                    <article class="news-item">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class=" col-md-12">
                                                    <div class="details">
                                                        <h5><a href="job_details.php?id=<?php echo $oneData->id?>"><?php echo $oneData->title?></a></h5>
                                                        <h6>Company: <?php echo $oneData->company_name?></h6>
                                                        <h6>Vacancy: <?php echo $oneData->vacancy?></h6>
                                                        <h6>Deadline: <?php echo $oneData->deadline?></h6>
                                                        <a href="job_details.php?id=<?php echo $oneData->id?>" class="btn btn-primary">See job details</a>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>


                                    </article><!--//news-item-->

                                    <?php
                                }
                                ?>
                            </div>

                        </section><!--//widget-->
                    </aside>
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>

<?php include("templateLayout/script/templateScript.php");?>

</body>
</html>

