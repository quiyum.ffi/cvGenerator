<?php
include_once "../vendor/autoload.php";
use App\Hobbies;
use App\Utility\Utility;
$object=new Hobbies();
$object->prepareData($_GET);
$object->deleteData();
return Utility::redirect('../profile.php');
