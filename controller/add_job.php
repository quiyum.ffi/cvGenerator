<?php
include_once "../vendor/autoload.php";
use App\Job_circular;
use App\Utility\Utility;
use App\Message\Message;
$object=new Job_circular();
$object->prepareData($_POST);
$object->store();
Message::setMessage("Job added successfully");
return Utility::redirect('../admin/job_circular.php');
