<?php
include_once "../vendor/autoload.php";
use App\New_field;
use App\Utility\Utility;
$object=new New_field();
$object->prepareData($_POST);
$object->updateData();
return Utility::redirect('../profile.php');
