<?php
include_once "../vendor/autoload.php";
use App\Language_communication;
use App\Utility\Utility;
$object=new Language_communication();
$_POST['new_language']=$_POST['language'];
$_POST['new_performance']=$_POST['performance'];
$_POST['new_criteria']=$_POST['criteria'];
$object->prepareData($_POST);
$object->updateData();
return Utility::redirect('../profile.php');
