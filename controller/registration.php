<?php
include_once "../vendor/autoload.php";
use App\Registration;
use App\Message\Message;
use App\Utility\Utility;
$object=new Registration();
if($_POST['password']==$_POST['c_password']){
    $object->prepareData($_POST);
    $object->insertData();
    Message::setMessage("Registration Successfully! Plz Login!");
    return Utility::redirect('../login.php');
}
else{
    Message::setMessage("Password Doesn't Match");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
