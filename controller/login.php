<?php
session_start();
require_once ("../vendor/autoload.php");
use App\Utility\Utility;
use App\Message\Message;
use App\Registration;
if($_POST['login_status']=='0'){
    $object=new Registration();
    $object->prepareData($_POST);
    $auth=$object->loginCheckAdmin();
    $userId=$object->userIdAdmin();
    if($auth){
        $_SESSION['role_status']=1;
        $_SESSION['email']=$_POST['email'];
        $_SESSION['user_id']=$userId->id;
        $_SESSION['user_name']=$userId->name;
        Utility::redirect('../admin/index.php');
    }
    else{
        Message::setMessage("Username and password doesn't match! Try Again");
        Utility::redirect('../login.php');

    }
}
else if($_POST['login_status']=='1'){

    $object=new Registration();
    $object->prepareData($_POST);
    $auth=$object->loginCheck();
    $userId=$object->userId();
    if($auth){
        $_SESSION['role_status']=0;
        $_SESSION['email']=$_POST['email'];
        $_SESSION['user_id']=$userId->id;
        $_SESSION['user_name']=$userId->name;
        Utility::redirect('../profile.php');
    }
    else{
        Message::setMessage("Username and password doesn't match! Try Again");
        Utility::redirect('../login.php');

    }
}

