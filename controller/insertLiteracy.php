<?php
include_once "../vendor/autoload.php";
use App\Computer_literacy;
use App\Utility\Utility;
$object=new Computer_literacy();
$object->prepareData($_POST);
$object->insertData();
return Utility::redirect($_SERVER['HTTP_REFERER']);
