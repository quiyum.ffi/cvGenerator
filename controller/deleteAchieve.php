<?php
include_once "../vendor/autoload.php";
use App\Achievement;
use App\Utility\Utility;
$object=new Achievement();
$object->prepareData($_GET);
$object->deleteData();
return Utility::redirect('../profile.php');
