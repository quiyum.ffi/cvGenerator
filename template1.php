<?php
session_start();
require_once("vendor/autoload.php");
include("templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
if($_SESSION['complete']<100){
    Utility::redirect('profile.php');
}
use App\User_info;
$object=new User_info();
$object->prepareData($_SESSION);
$exist_userInfo=$object->isInserted();
if($exist_userInfo){
    $userInfo=$object->showUserInfo();
}
use App\Education_qualification;
$objEdu=new Education_qualification();
$objEdu->prepareData($_SESSION);
$exist_academic=$objEdu->isInserted();
if($exist_academic){
    $academic=$objEdu->showAcademic();
}
use App\Computer_literacy;
$objLiteracy=new Computer_literacy();
$objLiteracy->prepareData($_SESSION);
$exist_literacy=$objLiteracy->isInserted();
if($exist_literacy){
    $oneLiteracy=$objLiteracy->showLiteracy();
}


use App\Hobbies;
$objHobbiy=new Hobbies();
$objHobbiy->prepareData($_SESSION);
$exist_hobbie=$objHobbiy->isInserted();
if($exist_hobbie){
    $oneHobbie=$objHobbiy->showHobbie();
}



use App\Language_communication;
$languageObj=new Language_communication();
$languageObj->prepareData($_SESSION);
$existLanguage=$languageObj->isInserted();

if($existLanguage){
    if(isset($_GET['language_id'])){
        $languageObj->prepareData($_GET);
        $oneLanguage=$languageObj->showOneData();
    }
    else{
        $allLanguage=$languageObj->showLanguage();
    }

}
use App\Category;
$catObj=new Category();
$catObj->prepareData($_GET);
$oneCategoryData=$catObj->oneData();

use App\New_field;
$fieldObj=new New_field();
$fieldObj->prepareData($_SESSION);
$exist_field=$fieldObj->isInserted();

if($exist_field){
    $allNewField=$fieldObj->showNew();
}
if(isset($_POST['edit_objective']) && !empty($_POST['edit_objective'])) {
    $_SESSION['edit_objective'] = $_POST['edit_objective'];
    $_SESSION['edit_proclamation'] = $_POST['edit_proclamation'];
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jquery.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div style="width: 100%;overflow: hidden">
        <div class="content container" style="width: 70%;overflow:hidden;float:left">

            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal">Edit Objective & Proclamation</button>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Objective</h4>
                        </div>
                        <div class="modal-body">
                            <form action="template1.php?category_id=<?php echo $_GET['category_id']?>" method="post">
                                <label>Objective</label>
                                <textarea rows="5" class="form-control" name="edit_objective"><?php echo $oneCategoryData->objective?></textarea>
                                <br>
                                <label>Proclamation</label>
                                <textarea rows="5" class="form-control" name="edit_proclamation"><?php echo $oneCategoryData->proclamation?></textarea>
                                <br>
                                <input type="submit" class="btn btn-primary" value="Edit">
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>




            <button onclick="ExportPdf()" class="btn btn-primary">Download as PDF</button>
            <div style="overflow: hidden;border: 1px black solid">
                <div style="width: 100%;font-family: 'Century Gothic'" id="myCanvas">
                    <div style="width:30%;float: left;margin-top: 70px;min-height: 1050px;">
                        <div style="margin: 0 auto;width: 60%">
                            <br>
                            <img src="resources/user_photos/<?php echo $userInfo->picture?>" width="100%" style="margin-top: 10px;border-radius: 5px">
                        </div>
                        <h4 class="text-center" style="color: #8DB3E2;font-weight: 600"><?php echo $userInfo->full_name?></h4>
                        <div style="margin-top: 150px">
                            <p style="text-align: center; color: #5f497a;font-size: 14px;">Mailing Address</p>
                            <p style="text-align: center; color: black"><?php echo $userInfo->present_address?></p>

                            <p style="text-align: center;color: black"><?php echo $userInfo->contact?></p>
                            <p style="text-align: center;color: black"><?php echo $_SESSION['email']?></p>
                        </div>
                        <div style="margin: 0 auto; width:80%">
                            <div style="margin-top: 55px">
                                <p style="text-align: center; color: #5f497a;font-size: 14px;">Career Objective</p>
                                <p style="text-align: center; color: black"><?php
                                    if(isset($_SESSION['edit_objective']) && !empty($_SESSION['edit_objective'])){
                                        echo $_SESSION['edit_objective'];
                                        $_SESSION['edit_objective']="";
                                    }
                                    else{
                                        echo $oneCategoryData->objective;
                                    }
                                    ?>
                                    </p>
                            </div>
                        </div>


                    </div>
                    <div style="width:70%;float: right;min-height: 1050px;">
                        <div style="margin-top: 70px">
                            <h4 class="text-center" style="color: #5F497A;font-weight: 600">Personal Information</h4>
                        </div>
                        <table style="width:80%;margin: 0 auto">
                            <tbody>
                            <tr>
                                <td style="width:30%">Name</td>
                                <td style="width:10%">:</td>
                                <td style="width:60%"><?php echo $userInfo->full_name?></td>
                            </tr>
                            <tr>
                                <td>Mother’s Name</td>
                                <td>:</td>
                                <td><?php echo $userInfo->mother_name?></td>
                            </tr>
                            <tr>
                                <td>Father’s Name</td>
                                <td>:</td>
                                <td><?php echo $userInfo->father_name?></td>
                            </tr>
                            <tr>
                                <td>Present Address</td>
                                <td>:</td>
                                <td><?php echo $userInfo->present_address?>
                                </td>
                            </tr>
                            <tr>
                                <td>Permanent Address</td>
                                <td>:</td>
                                <td><?php echo $userInfo->permanent_address?>
                                </td>
                            </tr>

                            <tr>
                                <td>Gender</td>
                                <td>:</td>
                                <td><?php echo $userInfo->gender?></td>
                            </tr>
                            <tr>
                                <td>Religion</td>
                                <td>:</td>
                                <td><?php echo $userInfo->religion?></td>
                            </tr>
                            <tr>
                                <td>National ID no</td>
                                <td>:</td>
                                <td><?php echo $userInfo->n_id?></td>
                            </tr>

                            </tbody>
                        </table>
                        <div style="margin-top: 50px">
                            <h4 class="text-center" style="color: #5F497A;font-weight: 600">Academic Credentials</h4>
                        </div>
                        <div style="width:80%;margin: 0 auto">
                            <?php
                            foreach ($academic as $oneData){
                                ?>
                                <h5><?php echo $oneData->degree?></h5>
                                <hr style="border-bottom:1px solid #444444;margin-top: -8px;margin-bottom: 10px">
                                <table >
                                    <tbody>
                                    <tr>
                                        <td style="width:30%">Institute</td>
                                        <td style="width:10%">:</td>
                                        <td style="width:60%"><?php echo $oneData->institute?></td>
                                    </tr>
                                    <tr>
                                        <td>Department/group</td>
                                        <td>:</td>
                                        <td><?php echo $oneData->department?></td>
                                    </tr>
                                    <tr>
                                        <td>Board</td>
                                        <td>:</td>
                                        <td><?php echo $oneData->board?></td>
                                    </tr>
                                    <tr>
                                        <td>Result</td>
                                        <td>:</td>
                                        <td><?php echo $oneData->cgpa ."(".$oneData->out_of.")"?></td>
                                    </tr>
                                    <tr>
                                        <td>Passing Year</td>
                                        <td>:</td>
                                        <td><?php echo $oneData->passing_year?></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>

                        </div>

                        <div style="margin-top: 50px">
                            <h4 class="text-center" style="color: #5F497A;font-weight: 600">Language Communication</h4>
                        </div>
                        <div style="width:80%;margin: 0 auto">
                            <table class="table table-borderd">
                                <tbody>
                                <tr>
                                    <td>Language</td>
                                    <td style="width: 30% ;">Performance</td>
                                    <td style="width: 45% ;">Written/Spoken</td>
                                </tr>
                                <?php
                                foreach ($allLanguage as $oneData){
                                    ?>

                                    <tr>
                                        <td style="width:60%"><?php echo $oneData->language?></td>
                                        <td style="width:10%"><?php echo $oneData->performance?></td>
                                        <td style="width:50%"><?php echo $oneData->critera?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="width:100%;float: right ;">

                        <div style="margin-top: 30px">
                            <h4 class="text-center" style="color: #5F497A;font-weight: 600">Computer Literacy</h4>
                        </div>
                        <div style="width:80%;margin: 0 auto">
                            <p style="text-align: center"><?php echo $oneLiteracy->literacy?></p>
                        </div>
                        <div style="margin-top: 30px">
                            <h4 class="text-center" style="color: #5F497A;font-weight: 600">Hobbies</h4>
                        </div>
                        <div style="width:80%;margin: 0 auto">
                            <p style="text-align: center"><?php echo $oneHobbie->hobbies?></p>
                        </div>

                        <?php
                        if($exist_field){
                            foreach ($allNewField as $oneData){
                                ?>
                                <div style="margin-top: 30px">
                                    <h4 class="text-center" style="color: #5F497A;font-weight: 600"><?php echo $oneData->title?></h4>
                                </div>
                                <div style="width:80%;margin: 0 auto">
                                    <p style="text-align: center"><?php echo $oneData->content?></p>
                                </div>
                                <?php
                            }
                        }

                        ?>
                        <div style="margin-top: 30px">
                            <h4 class="text-center" style="color: #5F497A;font-weight: 600">Proclamation</h4>
                        </div>
                        <div style="width:80%;margin: 0 auto">
                            <p><?php
                                if(isset($_SESSION['edit_proclamation']) && !empty($_SESSION['edit_proclamation'])){
                                    echo $_SESSION['edit_proclamation'];
                                    $_SESSION['edit_proclamation']="";
                                }
                                else{
                                    echo $oneCategoryData->proclamation;
                                }
                                ?>
                            </p>
                        </div>
                        <div style="margin-top: 50px">
                        </div>
                        <div style="width:80%;margin: 0 auto">
                            <p>Signature:_____________________________</p>
                            <p style="margin-left: 75px"><?php echo $userInfo->full_name?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--//content-->
        <div style="width: 20%;overflow: hidden;float: right;background: #080058;border-radius: 10px;margin-top: 30px;color: white;margin-right: 70px">

            <h4 class="text-center">Choose Template</h4>
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template1.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template1.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 1</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template2.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template2.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template2.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 2</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template3.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template3.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template3.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 3</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template4.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template4.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template4.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 4</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template5.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template5.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template5.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 5</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template6.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template6.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template6.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 6</h5></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="thumbnail">
                        <a href="template67.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template7.jpg" class="img-responsive"></a>
                    </div>
                    <div class="caption">
                        <a href="template7.php?category_id=<?php echo $_GET['category_id']?>"> <h5 style="text-align: center;color: white;">Template 7</h5></a>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div style="height: 50px"><br> </div>
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>


<script>
    function ExportPdf(){
        kendo.drawing
            .drawDOM("#myCanvas",
                {
                    paperSize: "A4",
                    margin: { top: "1cm", bottom: "1cm" },
                    scale: 0.8,
                    height: 400
                })
            .then(function(group){
                kendo.drawing.pdf.saveAs(group, "<?php echo $userInfo->full_name?>.pdf")
            });
    }
</script>


</body>
</html>

