<?php
/**
 * Created by PhpStorm.
 * User: FFI
 * Date: 10/19/2017
 * Time: 2:55 AM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;


class Education_qualification extends Database
{
    public $id;
    public $degree;
    public $user_id;
    public $institute;
    public $board;
    public $cgpa;
    public $out_of;
    public $passing_year;
    public $department;
    public $new_id;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data){
            if(array_key_exists('id',$data)){
                $this->id=$data['id'];
            }
            if(array_key_exists('new_degree',$data)){
                $this->degree=$data['new_degree'];
            }
            if(array_key_exists('new_institute',$data)){
                $this->institute=$data['new_institute'];
            }
            if(array_key_exists('new_department',$data)){
                $this->department=$data['new_department'];
            }
            if(array_key_exists('new_board',$data)){
                $this->board=$data['new_board'];
            }
            if(array_key_exists('new_cgpa',$data)){
                $this->cgpa=$data['new_cgpa'];
            }
            if(array_key_exists('new_out_of',$data)){
                $this->out_of=$data['new_out_of'];
            }
            if(array_key_exists('new_passing_year',$data)){
                $this->passing_year=$data['new_passing_year'];
            }
            if(array_key_exists('user_id',$data)){
                $this->user_id=$data['user_id'];
            }
            if(array_key_exists('update_academic_id',$data)){
                $this->new_id=$data['update_academic_id'];
            }
            return $this;
    }

    public function insertData(){

        $degree=explode("]]}",$this->degree);
        $institute=explode("]]}",$this->institute);
        $department=explode("]]}",$this->department);
        $board=explode("]]}",$this->board);
        $cgpa=explode("]]}",$this->cgpa);
        $out_of=explode("]]}",$this->out_of);
        $passing_year=explode("]]}",$this->passing_year);
        $count=0;
        foreach($degree as $pr){
            $query= "INSERT INTO `education_qualification`(`degree`, `institute`, `board`, `cgpa`, `out_of`, `passing_year`, `department`, `user_id`) VALUES (?,?,?,?,?,?,?,?)";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1,$degree[$count]);
            $STH->bindParam(2,$institute[$count]);
            $STH->bindParam(3,$board[$count]);
            $STH->bindParam(4,$cgpa[$count]);
            $STH->bindParam(5,$out_of[$count]);
            $STH->bindParam(6,$passing_year[$count]);
            $STH->bindParam(7,$department[$count]);
            $STH->bindParam(8,$this->user_id);
            $STH->execute();
            $count++;
        }
    }
    public function isInserted(){
        $query = "SELECT * FROM `education_qualification` WHERE `user_id`='$this->user_id'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();
        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function showAcademic(){
        $sql = "SELECT * FROM `education_qualification` WHERE `user_id`='$this->user_id' ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function showOneAcademic(){
        $sql = "SELECT * FROM `education_qualification` WHERE `id`='$this->new_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function updateData(){
        $query="UPDATE `education_qualification` SET `degree`=?,`institute`=?,`board`=?,`cgpa`=?,`out_of`=?,`passing_year`=?,`department`=? WHERE id='$this->id'";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->degree);
        $STH->bindParam(2,$this->institute);
        $STH->bindParam(3,$this->board);
        $STH->bindParam(4,$this->cgpa);
        $STH->bindParam(5,$this->out_of);
        $STH->bindParam(6,$this->passing_year);
        $STH->bindParam(7,$this->department);
        $STH->execute();
    }
    public function deleteData(){
        $query="DELETE FROM education_qualification WHERE id='$this->id'";
        $this->DBH->exec($query);
       
    }
}