<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 10/19/2017
 * Time: 11:22 PM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;


class Language_communication extends Database
{

    public $id;
    public $language;
    public $user_id;
    public $performance;
    public $critera;
    public $language_id;
    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        if (array_key_exists('new_language', $data)) {
            $this->language = $data['new_language'];
        }
        if (array_key_exists('new_performance', $data)) {
            $this->performance = $data['new_performance'];
        }
        if (array_key_exists('new_criteria', $data)) {
            $this->critera = $data['new_criteria'];
        }
        if (array_key_exists('language_id', $data)) {
            $this->language_id = $data['language_id'];
        }

        return $this;
    }

    public function insertData()
    {

        $language = explode("]]}", $this->language);
        $performance = explode("]]}", $this->performance);
        $critera = explode("]]}", $this->critera);

        $count = 0;
        foreach ($language as $pr) {
            $query = "INSERT INTO `language_communication` ( `user_id`, `language`, `performance`, `critera`) VALUES (?,?,?,?)";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1, $this->user_id);
            $STH->bindParam(2, $language[$count]);
            $STH->bindParam(3, $performance[$count]);
            $STH->bindParam(4, $critera[$count]);

            $STH->execute();
            $count++;
        }

    }
    public function showLanguage(){
        $sql = "SELECT * FROM `language_communication` WHERE `user_id`='$this->user_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function isInserted(){
        $query = "SELECT * FROM `language_communication` WHERE `user_id`='$this->user_id'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();
        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function updateData(){
        $query= "UPDATE language_communication SET `language`=?,`performance`=?,`critera`=? WHERE id='$this->id'";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->language);
        $STH->bindParam(2,$this->performance);
        $STH->bindParam(3,$this->critera);
        $STH->execute();

    }
    public function showOneData(){
        $sql = "SELECT * FROM `language_communication` WHERE `id`='$this->language_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function deleteData(){
        $query="DELETE FROM language_communication WHERE id='$this->id'";
        $this->DBH->exec($query);

    }

}