<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 11/13/2017
 * Time: 1:03 AM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Category extends Database
{
    public $id;
    public $category_name;
    public $objective;
    public $proclamation;
    public $status;
    public $picture;
    public function __construct()
    {
        parent::__construct();
    }
    public function prepareData($data){
        if(array_key_exists('category_id',$data)){
            $this->id=$data['category_id'];
        }
        if(array_key_exists('category_name',$data)){
            $this->category_name=$data['category_name'];
        }
        if(array_key_exists('objective',$data)){
            $this->objective=$data['objective'];
        }
        if(array_key_exists('proclamation',$data)){
            $this->proclamation=$data['proclamation'];
        }
        if(array_key_exists('pic',$data)){
            $this->picture=$data['pic'];
        }
    }
    public function insertData(){

        $query="INSERT INTO `category`(`category_name`, `objective`, `proclamation`, `pic`) VALUES (?,?,?,?)";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->category_name);
        $STH->bindParam(2,$this->objective);
        $STH->bindParam(3,$this->proclamation);
        $STH->bindParam(4,$this->picture);
        $STH->execute();
    }
    public function update(){

        $query="UPDATE `category` SET `category_name`=?,`objective`=?,`proclamation`=? WHERE id='$this->id'";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->category_name);
        $STH->bindParam(2,$this->objective);
        $STH->bindParam(3,$this->proclamation);
        $STH->execute();
    }
    public function deleteData(){
        $this->status=1;
        $query="UPDATE `category` SET `status`=? WHERE id='$this->id'";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->status);
        $STH->execute();
    }

    public function updatePic(){

        $query="UPDATE `category` SET pic=? WHERE id='$this->id'";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->picture);
        $STH->execute();
    }
    public function allCategory(){
        $sql = "SELECT * FROM `category` WHERE status='0'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }
    public function oneData(){
        $sql = "SELECT * FROM `category` WHERE id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }
}