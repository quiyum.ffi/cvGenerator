<?php
/**
 * Created by PhpStorm.
 * User: FFI
 * Date: 10/17/2017
 * Time: 2:35 AM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;


class User_info extends Database
{
    public $id;
    public $name;
    public $user_id;
    public $father_name;
    public $mother_name;
    public $present_address;
    public $permanent_address;
    public $contact;
    public $n_id;
    public $picture;
    public $gender;
    public $religion;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function prepareData($data){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('full_name',$data)){
            $this->name=$data['full_name'];
        }
        if(array_key_exists('father',$data)){
            $this->father_name=$data['father'];
        }
        if(array_key_exists('present',$data)){
            $this->present_address=$data['present'];
        }
        if(array_key_exists('gender',$data)){
            $this->gender=$data['gender'];
        }
        if(array_key_exists('contact',$data)){
            $this->contact=$data['contact'];
        }
        if(array_key_exists('mother',$data)){
            $this->mother_name=$data['mother'];
        }
        if(array_key_exists('permanent',$data)){
            $this->permanent_address=$data['permanent'];
        }
        if(array_key_exists('religion',$data)){
            $this->religion=$data['religion'];
        }
        if(array_key_exists('nid',$data)){
            $this->n_id=$data['nid'];
        }
        if(array_key_exists('user_id',$data)){
            $this->user_id=$data['user_id'];
        }
        if(array_key_exists('pic',$data)){
            $this->picture=$data['pic'];
        }
        return $this;
    }

    public function insertData(){
        $query="INSERT INTO `user_info`(`full_name`, `user_id`, `father_name`, `mother_name`, `present_address`, `permanent_address`, `contact`, `n_id`, `picture`, `gender`, `religion`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->name);
        $STH->bindParam(2,$this->user_id);
        $STH->bindParam(3,$this->father_name);
        $STH->bindParam(4,$this->mother_name);
        $STH->bindParam(5,$this->present_address);
        $STH->bindParam(6,$this->permanent_address);
        $STH->bindParam(7,$this->contact);
        $STH->bindParam(8,$this->n_id);
        $STH->bindParam(9,$this->picture);
        $STH->bindParam(10,$this->gender);
        $STH->bindParam(11,$this->religion);
        $STH->execute();
    }


    public function updateData(){
        $query="UPDATE `user_info` SET `full_name`=?,`father_name`=?,`mother_name`=?,`present_address`=?,`permanent_address`=?,`contact`=?,`n_id`=?,`gender`=?,`religion`=? WHERE `user_id`='$this->user_id'";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->name);
        $STH->bindParam(2,$this->father_name);
        $STH->bindParam(3,$this->mother_name);
        $STH->bindParam(4,$this->present_address);
        $STH->bindParam(5,$this->permanent_address);
        $STH->bindParam(6,$this->contact);
        $STH->bindParam(7,$this->n_id);
        $STH->bindParam(8,$this->gender);
        $STH->bindParam(9,$this->religion);
        $STH->execute();
    }
    public function updatePicture(){
        $query="UPDATE `user_info` SET `picture`=? WHERE `user_id`='$this->user_id'";
        $STH=$this->DBH->prepare($query);
        $STH->bindParam(1,$this->picture);
        $STH->execute();
    }
    public function isInserted(){
        $query = "SELECT * FROM `user_info` WHERE `user_id`='$this->user_id'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();
        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function showUserInfo(){
        $sql = "SELECT * FROM `user_info` WHERE `user_id`='$this->user_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

}