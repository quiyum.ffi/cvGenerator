<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 11/15/2017
 * Time: 2:24 AM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class New_field extends Database
{
    public $id;
    public $user_id;
    public $title;
    public $content;
    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('user_id',$data)){
            $this->user_id=$data['user_id'];
        }
        if(array_key_exists('field',$data)){
            $this->title=$data['field'];
        }
        if(array_key_exists('content',$data)){
            $this->content=$data['content'];
        }

        return $this;
    }


    public function newField()
    {
        $query = "INSERT INTO `new_field` ( `user_id`, `title`, `content`) VALUES (?,?,?)";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1, $this->user_id);
        $STH->bindParam(2, $this->title);
        $STH->bindParam(3, $this->content);
        $STH->execute();
    }
    public function updateData()
    {
        $query = "UPDATE `new_field` SET `title`=?,`content`=? WHERE id='$this->id'";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1, $this->title);
        $STH->bindParam(2, $this->content);
        $STH->execute();
    }
    public function showNew(){
        $sql = "SELECT * FROM `new_field` WHERE `user_id`='$this->user_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function isInserted(){
        $query = "SELECT * FROM `new_field` WHERE `user_id`='$this->user_id'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();
        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function deleteData(){
        $query="DELETE FROM new_field WHERE id='$this->id'";
        $this->DBH->exec($query);

    }
}