<?php
session_start();
require_once("vendor/autoload.php");
include("templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
if($_SESSION['complete']<100){
    Utility::redirect('profile.php');
}
use App\Category;
$object=new Category();
$allData=$object->allCategory();

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left"> Generate CV</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current"> Generate CV</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                    }

                    ?>
                    <div class="col-md-12 col-sm-12  col-xs-12 ">
                        <?php
                            if(isset($_POST['category_id']) && !empty($_POST['category_id'])){
                                ?>
                                <article class="contact-form col-md-12 col-sm-12 col-xs-12  page-row">

                                        <div class="col-md-4">
                                            <div class="thumbnail">
                                                <a href="template1.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template.jpg" class="img-responsive"></a>
                                            </div>
                                            <div class="caption">
                                                <a href="template1.php?category_id=<?php echo $_POST['category_id']?>"> <h5 style="text-align: center">Template 1</h5></a>
                                            </div>
                                        </div>
                                    <div class="col-md-4">
                                        <div class="thumbnail">
                                            <a href="template2.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template2.jpg" class="img-responsive"></a>
                                        </div>
                                        <div class="caption">
                                            <a href="template2.php?category_id=<?php echo $_POST['category_id']?>"> <h5 style="text-align: center">Template 2</h5></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="thumbnail">
                                            <a href="template3.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template3.jpg" class="img-responsive"></a>
                                        </div>
                                        <div class="caption">
                                            <a href="template3.php?category_id=<?php echo $_POST['category_id']?>"> <h5 style="text-align: center">Template 3</h5></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="thumbnail">
                                            <a href="template4.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template4.jpg" class="img-responsive"></a>
                                        </div>
                                        <div class="caption">
                                            <a href="template4.php?category_id=<?php echo $_POST['category_id']?>"> <h5 style="text-align: center">Template 4</h5></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="thumbnail">
                                            <a href="template5.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template5.jpg" class="img-responsive"></a>
                                        </div>
                                        <div class="caption">
                                            <a href="template5.php?category_id=<?php echo $_POST['category_id']?>"> <h5 style="text-align: center">Template 5</h5></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="thumbnail">
                                            <a href="template6.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template6.jpg" class="img-responsive"></a>
                                        </div>
                                        <div class="caption">
                                            <a href="template6.php?category_id=<?php echo $_POST['category_id']?>"> <h5 style="text-align: center">Template 6</h5></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="thumbnail">
                                            <a href="template7.php?category_id=<?php echo $_POST['category_id']?>"><img src="resources/img/template7.jpg" class="img-responsive"></a>
                                        </div>
                                        <div class="caption">
                                            <a href="template7.php?category_id=<?php echo $_POST['category_id']?>"> <h5 style="text-align: center">Template 7</h5></a>
                                        </div>
                                    </div>

                                </article><!--//contact-form-->
                        <?php
                            }
                        else{
                            ?>
                            <article class="contact-form col-md-12 col-sm-12 col-xs-12  page-row">
                                <?php
                                foreach ($allData as $oneData){
                                    ?>
                                    <div class="col-md-3">
                                        <div class="thumbnail">
                                            <form action="generate_cv.php" method="post">
                                                <input type="hidden" name="category_id" value="<?php echo $oneData->id?>">
                                                <button name="action" type="submit"><img src="resources/category_pic/<?php echo $oneData->pic?>" class="img-responsive"></button>
                                            </form>
                                        </div>
                                        <div class="caption">
                                            <h5 style="text-align: center"><?php echo $oneData->category_name?></h5>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </article><!--//contact-form-->
                        <?php
                        }
                        ?>

                    </div>

                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>

<?php include("templateLayout/script/templateScript.php");?>

</body>
</html>

