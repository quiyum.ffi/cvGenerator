<?php
session_start();
require_once("vendor/autoload.php");
include("templateLayout/templateInformation.php");
use App\Registration;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Registration();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('login.php');
}
use App\Job_circular;
$object=new Job_circular();
$allData=$object->show();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">All Jobs</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">All Jobs</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row page-row">

                    <div class="news-wrapper col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php
                                foreach ($allData as $data){
                                    $post_date=date("d-M-Y", strtotime("$data->post_date"));
                                    $deadline=date("d-M-Y", strtotime("$data->deadline"));
                                    ?>
                                    <div class="col-md-4 col-sm-4 col-xs-6">
                                        <div class="item">
                                            <div class="news-item">
                                                <div style="min-height: 50px">
                                                    <h4 class="title text-center"><strong><a href="job_details.php?id=<?php echo $data->id?>"><br><?php echo $data->title?></a></strong></h4>
                                                </div>

                                                <p class="text-center">Posted: <?php echo $post_date;?></p>
                                                <h5 class="text-center"><strong>Company: <a><?php echo $data->company_name?></a></strong></h5>
                                                <h6 class="text-center">Post: <a><?php echo $data->post?></a></h6>
                                                <h6 class="text-center">Deadline: <a><?php echo $deadline?></a></h6>
                                                <h6 class="text-center">Contact: <a><?php echo $data->company_contact?></a></h6>
                                                <a class="btn btn-primary read-more" style="width: 100%" href="job_details.php?id=<?php echo $data->id?>">See full details<i class="fa fa-chevron-right"></i></a>
                                                <br>
                                            </div><!--//news-item-->

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div><!--//news-wrapper-->

                </div><!--//page-row-->
            </div>
        </div><!--//page-wrapper-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>

<?php include("templateLayout/script/templateScript.php");?>

</body>
</html>

